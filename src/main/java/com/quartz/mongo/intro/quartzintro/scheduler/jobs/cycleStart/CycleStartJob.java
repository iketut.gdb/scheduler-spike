package com.quartz.mongo.intro.quartzintro.scheduler.jobs.cycleStart;

import com.quartz.mongo.intro.quartzintro.scheduler.jobs.SampleJob;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.io.IOException;

public class CycleStartJob extends QuartzJobBean {

    private static Logger log = LoggerFactory.getLogger(SampleJob.class);

    private ApplicationContext applicationContext;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        CloseableHttpClient client = HttpClientBuilder.create().build();
        try {
            CloseableHttpResponse response = client.execute(new HttpGet("http://localhost:27017"));
            String bodyAsString = EntityUtils.toString(response.getEntity());
            log.info(bodyAsString);
        } catch (IOException  e) {
            e.printStackTrace();
        }
    }
}
