package com.quartz.mongo.intro.quartzintro.scheduler.template;

import com.quartz.mongo.intro.quartzintro.scheduler.jobs.cycleStart.CycleStartJob;
import com.quartz.mongo.intro.quartzintro.utils.ServiceCreator;
import org.quartz.Scheduler;
import org.quartz.Trigger;

public abstract class ServiceTemplate extends ServiceCreator {
    public ServiceTemplate(Scheduler scheduler, String pollingJobKey, String pollingJobGroup, String pollingTriggerKey, Class<CycleStartJob> jobClass) {
        super(scheduler, pollingJobKey, pollingJobGroup, pollingTriggerKey, jobClass);
    }

    public abstract Trigger triggerJobWithSettings();
}
