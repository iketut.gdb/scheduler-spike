package com.quartz.mongo.intro.quartzintro.utils;

import com.quartz.mongo.intro.quartzintro.scheduler.jobs.cycleStart.CycleStartJob;
import org.quartz.*;
import org.quartz.impl.JobDetailImpl;

public abstract class ServiceCreator {

    private Scheduler scheduler;
    protected Trigger trigger;
    private String pollingJobKey;
    protected String pollingJobGroup;
    protected String pollingTriggerKey;
    protected Class<CycleStartJob> jobClass;

    public ServiceCreator(Scheduler scheduler) {
        this.scheduler = scheduler;
    }
    public ServiceCreator(Scheduler scheduler, String pollingJobKey, String pollingJobGroup, String pollingTriggerKey, Class<CycleStartJob> jobClass) {
        this.scheduler = scheduler;
        this.jobClass = jobClass;
        this.pollingJobKey = pollingJobKey;
        this.pollingJobGroup = pollingJobGroup;
        this.pollingTriggerKey = pollingTriggerKey;
    }

    public JobDetail generateJobDetail() {
        return JobDetailCreator.createJobDetail(jobClass, pollingJobKey, pollingJobGroup);
    }

    public void setPollingJobKey(String pollingJobKey) {
        this.pollingJobKey = pollingJobKey;
    }

    public void setPollingJobGroup(String pollingJobGroup) {
        this.pollingJobGroup = pollingJobGroup;
    }

    public void setPollingTriggerKey(String pollingTriggerKey) {
        this.pollingTriggerKey = pollingTriggerKey;
    }

    public void setTrigger(Trigger trigger) {
        this.trigger = trigger;
    }

    public void setJobClass(Class<CycleStartJob> jobClass) {
        this.jobClass = jobClass;
    }

    private JobDetail implementJobDetail() {
        JobDetailImpl jobDetail = new JobDetailImpl();
        jobDetail.setKey(
                new JobKey(pollingJobKey, pollingJobGroup));
        jobDetail.setJobClass(jobClass);
        jobDetail.setDurability(true);
        return jobDetail;
    }

    public void start() throws SchedulerException {
        final JobDetail assignedJob = implementJobDetail();

        this.scheduler.addJob(assignedJob, true, true);
        if (!this.scheduler.checkExists(new TriggerKey(
                pollingTriggerKey, pollingJobGroup))) {
            this.scheduler.scheduleJob(trigger);
        }
    }
}
