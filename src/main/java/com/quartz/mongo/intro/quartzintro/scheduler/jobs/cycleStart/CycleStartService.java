package com.quartz.mongo.intro.quartzintro.scheduler.jobs.cycleStart;

import com.quartz.mongo.intro.quartzintro.constants.SchedulerConstants;
import com.quartz.mongo.intro.quartzintro.scheduler.template.ServiceTemplate;
import org.quartz.Scheduler;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static org.quartz.TriggerBuilder.newTrigger;

public class CycleStartService extends ServiceTemplate {

    public CycleStartService(Scheduler scheduler) {
        super(scheduler, SchedulerConstants.SAMPLE_JOB_POLLING_JOB_KEY, SchedulerConstants.SAMPLE_JOB_POLLING_GROUP, SchedulerConstants.SAMPLE_JOB_POLLING_TRIGGER_KEY, CycleStartJob.class);
        this.setTrigger(this.triggerJobWithSettings());
    }

    @Override
    public Trigger triggerJobWithSettings() {
        return newTrigger().forJob(generateJobDetail())
                .withIdentity(this.pollingTriggerKey, this.pollingJobGroup)
                .withPriority(50).withSchedule(SimpleScheduleBuilder.repeatMinutelyForever())
                .startAt(Date.from(LocalDateTime.now().plusSeconds(3).atZone(ZoneId.systemDefault()).toInstant()))
                .build();
    }
}
