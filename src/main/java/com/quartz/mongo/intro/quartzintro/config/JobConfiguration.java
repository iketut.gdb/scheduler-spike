package com.quartz.mongo.intro.quartzintro.config;

import static org.quartz.TriggerBuilder.newTrigger;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.annotation.PostConstruct;

import com.quartz.mongo.intro.quartzintro.scheduler.jobs.cycleStart.CycleStartService;
import com.quartz.mongo.intro.quartzintro.utils.JobDetailCreator;
import org.quartz.*;
import org.quartz.impl.JobDetailImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import com.quartz.mongo.intro.quartzintro.constants.SchedulerConstants;
import com.quartz.mongo.intro.quartzintro.scheduler.jobs.SampleJob;

/**
 * This will configure the job to run within quartz.
 *
 * @author dinuka
 */
@Configuration
public class JobConfiguration {

    @Autowired
    private SchedulerFactoryBean schedulerFactoryBean;


    @PostConstruct
    private void initialize() throws Exception {
        Scheduler quartzScheduler = schedulerFactoryBean.getScheduler();

        CycleStartService cycleStartService = new CycleStartService(quartzScheduler);
        cycleStartService.start();
    }

    /**
     * <p>
     * The job is configured here where we provide the job class to be run on
     * each invocation. We give the job a name and a value so that we can
     * provide the trigger to it on our method {@link #sampleJobTrigger()}
     * </p>
     *
     * @return an instance of {@link JobDetail}
     */
    private static JobDetail sampleJobDetail() {
        JobDetailImpl jobDetail = new JobDetailImpl();
        jobDetail.setKey(
                new JobKey(SchedulerConstants.SAMPLE_JOB_POLLING_JOB_KEY, SchedulerConstants.SAMPLE_JOB_POLLING_GROUP));
        jobDetail.setJobClass(SampleJob.class);
        jobDetail.setDurability(true);
        return jobDetail;
    }

    /**
     * <p>
     * This method will define the frequency with which we will be running the
     * scheduled job which in this instance is every minute three seconds after
     * the start up.
     * </p>
     *
     * @return an instance of {@link Trigger}
     */
    private static Trigger sampleJobTrigger() {
        return newTrigger().forJob(sampleJobDetail())
                .withIdentity(SchedulerConstants.SAMPLE_JOB_POLLING_TRIGGER_KEY,
                        SchedulerConstants.SAMPLE_JOB_POLLING_GROUP)
                .withPriority(50).withSchedule(SimpleScheduleBuilder.repeatMinutelyForever())
                .startAt(Date.from(LocalDateTime.now().plusSeconds(3).atZone(ZoneId.systemDefault()).toInstant()))
                .build();
    }

}
