package com.quartz.mongo.intro.quartzintro.scheduler.jobs;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import com.quartz.mongo.intro.quartzintro.config.JobConfiguration;
import com.quartz.mongo.intro.quartzintro.config.QuartzConfiguration;

import java.io.IOException;

/**
 *
 * This is the job class that will be triggered based on the job configuration
 * defined in {@link JobConfiguration}
 *
 * @author dinuka
 *
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class SampleJob extends QuartzJobBean {

	private static Logger log = LoggerFactory.getLogger(SampleJob.class);

	private ApplicationContext applicationContext;

	/**
	 * This is the method that will be executed each time the trigger is fired.
	 */
	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		log.info("This is the sample job, executed by {}", applicationContext.getBean(Environment.class));
		CloseableHttpClient client = HttpClientBuilder.create().build();
		try {
			CloseableHttpResponse response = client.execute(new HttpGet("http://localhost:27017"));
			String bodyAsString = EntityUtils.toString(response.getEntity());
			log.info(bodyAsString);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
