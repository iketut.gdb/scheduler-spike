package com.quartz.mongo.intro.quartzintro.utils;

import com.quartz.mongo.intro.quartzintro.constants.SchedulerConstants;
import com.quartz.mongo.intro.quartzintro.scheduler.jobs.SampleJob;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.impl.JobDetailImpl;

public class JobDetailCreator {
    public static JobDetail createJobDetail(Class<? extends Job> jobClass, String name) {
        JobDetailImpl jobDetail = new JobDetailImpl();
        jobDetail.setKey(new JobKey(name));
        jobDetail.setJobClass(jobClass);
        jobDetail.setDurability(false);

        return jobDetail;
    }

    public static JobDetail createJobDetail(Class<? extends Job> jobClass, String name, String groupName) {
        JobDetailImpl jobDetail = new JobDetailImpl();
        jobDetail.setKey(new JobKey(name, groupName));
        jobDetail.setJobClass(jobClass);
        jobDetail.setDurability(false);

        return jobDetail;
    }
}
